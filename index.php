<?php
/**
 * Created by PhpStorm.
 * User: Mehedee Hasan
 * Date: 2/17/14
 * Time: 4:50 AM
 */
session_start();
//error_reporting(0);
include('server_connect.php');

if(!empty($_SESSION['error'])){
    $warning='<div style="color: white; border: 5px solid green; padding: 3px;">' .$_SESSION['error'] . '</div>';
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery-2.1.0.js"></script>
    <title>Home</title>
</head>
<body>

<div class="header">
    <h1>
        Basic CRUD
    </h1>
    <p>
        Attempting a Basic CRUD App.
    </p>
</div>

<div id="reg_log">
    <?php
    if(!empty($_SESSION['error'])){
        echo $warning;
        unset($warning);
        unset($_SESSION['error']);
    }
    ?>
    <!---Register Page--->

    <div id="registration">
        <h1 id="box"><span>Sign Up</span></h1>

        <form action="registration.php" method="post" id="form1">

            <input type="text" name="reg_name" placeholder="User Name" required /> <br><br>
            <input type="password" name="reg_pass" placeholder="Choose A Password" required> <br><br>
<!--            <input type="password" name="reg_pass_val" placeholder="Password again" <br> <br>-->
            <input type="email" name="reg_email" placeholder="Your Email Address" required> <br> <br>
            <input type="submit" value="Register" style="width:140px; font-weight: bold; ">
            <input type="reset" value="NeverMind" style="width:140px; font-weight: bold; " > <br>
        </form>
    </div>

    <script>
        $(document).ready(function(){
            $("#registration").children("h1").on('click',function(){
                $("#form1").slideDown(600);
                $("#box").animate({ height: 40 }, 600, function(){
                    $("#box").css({'background-color': 'transparent', 'display': 'none'});
//                    $("#registration").css('border', '1px solid red');
                    $("#registration").css('background-color', '#3299ff');
                });
            });
            $("[type=reset]").on('click', function(){
                $("#form1").slideUp(600);
                $("#registration").html("It's Okay!");
            })

        });
    </script>


    <!---Login Page--->
    <div id="login">
        <form action="" method="post">
            <h2>Log In</h2>
            <input type="text" name="user_name" placeholder="User Name" required> <br><br>
            <input type="password" name="user_password" placeholder="Password" required> <br> <br>
            <input type="submit" value="Log In" style="width:130px; font-weight:bold;"> <br>

        </form>
    </div>
</div>

</body>
</html>
